<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script src="<?php echo plugin_dir_url('ics_pg/content/resources/bootstrap.min.js',__FILE__);?>/bootstrap.min.js"></script>

<script type="text/javascript">

    $(function(){
        $('#submit').click(function(){
            $('#myModal').modal();
        })
    });

</script>
<style>
    table#agree td {
        padding: 6px;
        border: 1px solid rgba(51, 122, 183, 0.04);
    }
</style>
<form action="https://ebanking.bankofmaldives.com.mv/bmlmpiuat/threed/MPI" method="post">
	<input type="hidden" value="1.0.0" name="Version" id="Version">
	<input type="hidden" value="<?php echo get_option('ics_pg_merchant_id'); ?>" name="MerID" id="MerID">
	<input type="hidden" value="<?php echo get_option('ics_pg_acquirer_id'); ?>" name="AcqID" id="AcqID">
	<input type="hidden" value="<?php echo get_option('ics_pg_cb_url'); ?>" name="MerRespURL" id="MerRespURL">
	<input type="hidden" value="462" name="PurchaseCurrency" id="PurchaseCurrency">
	<input type="hidden" value="2" name="PurchaseCurrencyExponent" id="PurchaseCurrencyExponent">
	<input type="hidden" value="<?php echo $data->bml->getOrderId() ?>" name="OrderID" id="OrderID">
	<input type="hidden" value="SHA1" name="SignatureMethod" id="SignatureMethod">
	<input type="hidden" value="<?php echo $data->bml->getAmount(); ?>" name="PurchaseAmt" id="PurchaseAmt">
	<input type="hidden" value="https://ebanking.bankofmaldives.com.mv/bmlmpiuat/threed/MPI" name="Url" id="Url">
	<input type="hidden" value="<?php echo $data->bml->calculateSignature(); ?>" name="Signature" id="Signature">
    <p>
        <?php echo $data->bml->calculateSignatureX(); ?>
        
    </p>
	<h2>Payment Agreement</h2>
    <hr>
    <h5>You selected the following package</h5>
    <table id="agree">
        <tr>
            <td width="200">Amount:</td>
            <td>MVR<?php echo $data->bml->getRealAmount();?></td>
        </tr>

        <tr>
            <td width="200">Number of Users:</td>
            <td><?php echo $data->package['no_users'];?></td>
        </tr>
    </table>
    <br>
    <input type="button" id="submit" value="Proceed" class="btn btn-primary btn-lg">
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Attention!</h4>
                </div>
                <div class="modal-body">
                    <p>

                        <h4 style="
                            background: rgba(255, 0, 0, 0.67);
                            padding: 10px;
                            ">
                            Upon clicking 'proceed', you will be taken to the BML gateway!
                        </h4>

                    </p>



                    Once the transaction is completed, you will be automatically taken back to this page.

                    Please DO NOT click Refresh or Back button. <img height="20" src="<?php echo plugins_url('/back.png',__FILE__); ?>"> <img height="20" src="<?php echo plugins_url('/refresh.png',__FILE__); ?>" alt="">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Proceed</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</form>



