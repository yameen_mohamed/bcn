


<style type="text/css">
	/* Styles for Pricing Tables */


	
	h1 {color:#2c3e50;}
	h4 a {color:#ff9715; text-decoration:none;}
	h4 a:hover{color:#2c3e50;}

	.tiny {
		margin:20px 0;background:url(
			data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAIklEQVQIW2NkQAIfP378zwjjgzj8/PyMYAEYB8RmROaABAAVMg/XkcvroQAAAABJRU5ErkJggg==   ) repeat;

		-moz-box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		-webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		border:1px solid #14937a;
		border-bottom:10px solid #14937a;

	}

	.small {
		margin:20px 0;background:url(
			data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAIklEQVQIW2NkQAIfP378zwjjgzj8/PyMYAEYB8RmROaABAAVMg/XkcvroQAAAABJRU5ErkJggg==   ) repeat;

		-moz-box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		-webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		border:1px solid #cd8109;
		border-bottom:10px solid #cd8109;

	}

	.medium {
		margin:20px 0;background:url(
			data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAIklEQVQIW2NkQAIfP378zwjjgzj8/PyMYAEYB8RmROaABAAVMg/XkcvroQAAAABJRU5ErkJggg==   ) repeat;

		-moz-box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		-webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		border:1px solid #a12f23;
		border-bottom:10px solid #a12f23;


	}

	.pro {
		margin:20px 0;background:url(
			data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAIklEQVQIW2NkQAIfP378zwjjgzj8/PyMYAEYB8RmROaABAAVMg/XkcvroQAAAABJRU5ErkJggg==   ) repeat;

		-moz-box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		-webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		border:1px solid #1d6391;
		border-bottom:10px solid #1d6391;


	}

	.ten {
		margin: 20px 0;
		background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAIklEQVQIW2NkQAIfP378zwjjgzj8/PyMYAEYB8RmROaABAAVMg/XkcvroQAAAABJRU5ErkJggg== ) repeat;
		-moz-box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		-webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		box-shadow: 0 2px 5px 0 rgba(0,0,0,.2);
		border: 1px solid #00077B;
		border-bottom: 10px solid #00077B;
	}
	.pricing-table strong { font-weight: 700; color: #3d3d3d; }

	.pricing-table-header-tiny {
		padding: 5px 0 5px 0;
		background: #16a085;
		border-bottom:10px solid #14937a;
	}

	.pricing-table-header-small {
		padding: 5px 0 5px 0;
		background: #f39c12;
		border-bottom:10px solid #cd8109;

	}

	.pricing-table-header-medium {
		padding: 5px 0 5px 0;
		background: #c0392b;
		border-bottom:10px solid #a12f23;

	}

	.pricing-table-header-pro {
		padding: 5px 0 5px 0;
		background: #2980b9;
		border-bottom:10px solid #1d6391;
	}

	.pricing-table-header-ten {
		padding: 5px 0 5px 0;
		background: #00077B;
		border-bottom: 10px solid #00055D;
	}

	.pricing-table-header-tiny h2, .pricing-table-header-small h2, .pricing-table-header-medium h2, .pricing-table-header-pro h2, .pricing-table-header-ten h2 {  font-weight: 700; color:#FFF; }
	.pricing-table-header-tiny h3, .pricing-table-header-small h3, .pricing-table-header-medium h3, .pricing-table-header-pro h3, .pricing-table-header-ten h3 { font-weight: 500; color:#FFF;  }


	.pricing-table-text {
		margin: 15px 30px 0 30px;
		padding: 0 10px 15px 10px;
		border-bottom: 1px solid #ddd;
		text-align: left;
		line-height: 30px;
		font-size: 16px;
		color: #888;
	}

	.pricing-table-features {
		margin: 15px 10px 0 10px;
		padding: 0 10px 15px 10px;
		border-bottom: 1px dashed #888;
		text-align: center;
		line-height: 20px;
		font-size: 14px;
		color: #888;
	}

	.pricing-table-signup-tiny {
		margin-top: 25px;
		padding-bottom: 10px;
	}

	.pricing-table-signup-tiny input {
		border:0;
		display: inline-block;
		width:50%;
		height: 50px;
		background: #16a085;
		line-height: 50px;

		color: #fff;
		text-decoration: none;
		text-transform: uppercase;
	}

	.pricing-table-signup-tiny input:hover {
		text-decoration: none;
		background: #14937a;
		color: #fff;


	}

	.pricing-table-signup-tiny input:active {
		background: #14937a;
		color: #fff;
	}


	.pricing-table-signup-small {
		margin-top: 25px;
		padding-bottom: 10px;
	}

	.pricing-table-signup-small input {
		border:0;
		display: inline-block;
		width:50%;
		height: 50px;
		background: #f39c12;
		line-height: 50px;

		color: #fff;
		text-decoration: none;
		text-transform: uppercase;

	}

	.pricing-table-signup-small input:hover {
		text-decoration: none;
		background-color:#cd8109;
		color: #fff;
	}

	.pricing-table-signup-small input:active {
		background-color:#cd8109;
		color: #fff;

	}

	.pricing-table-signup-medium {
		margin-top: 25px;
		padding-bottom: 10px;
	}

	.pricing-table-signup-medium input {
		border:0;
		display: inline-block;
		width:50%;
		height: 50px;
		background: #c0392b;
		line-height: 50px;

		color: #fff;
		text-decoration: none;
		text-transform: uppercase;

	}

	.pricing-table-signup-medium input:hover {
		text-decoration: none;
		background-color:#a12f23;
		color:#FFF;
	}

	.pricing-table-signup-medium input:active {
		background-color:#a12f23;
		color: #fff;

	}

	.pricing-table-signup-pro {
		margin-top: 25px;
		padding-bottom: 10px;
	}

	.pricing-table-signup-pro input {
		border:0;
		display: inline-block;
		width:50%;
		height: 50px;
		background: #2980b9;
		line-height: 50px;

		color: #fff;
		text-decoration: none;
		text-transform: uppercase;
	}

	.pricing-table-signup-pro input:hover {
		text-decoration: none;
		background-color:#1d6391;
		color: #fff;
	}

	.pricing-table-signup-pro input:active {
		background-color:#1d6391;
		color: #fff;

	}


	.pricing-table-signup-ten {
		margin-top: 25px;
		padding-bottom: 10px;
	}

	.pricing-table-signup-ten input:hover {
		text-decoration: none;
		background-color: #00055D;
		color: #fff;
	}
	.pricing-table-signup-ten input {
		border:0;
		display: inline-block;
		width: 50%;
		height: 50px;
		background: #00077B;
		line-height: 50px;
		color: #fff;
		text-decoration: none;
		text-transform: uppercase;
	}

	.pricing-table-signup-ten a:active {
		background-color:#1d6391;
		color: #fff;

	}

	.top {margin-top:20px;}

	.space {margin:10px;}

	.center {text-align: center;}

</style>


<form action="<?php echo $_SERVER['REQUEST_URI'];  ?>" method="post">
	<h2>Please enter your email address and select the desired package to purchase license.</h2>
	<br>
	<div class="row-fluid">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label for="exampleInputEmail1">Email Address</label>
					<input type="email" name="email" required="required" value="<?php echo $_POST['email']; ?>" class="form-control" id="exampleInputEmail1" placeholder="Email">
				</div>
            </div>
        </div>

        <div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label for="exampleInputEmail1">Confirm Email Address</label>
					<input type="email" name="confirm_email" value="<?php echo $_POST['confirm_email']; ?>" required="required" class="form-control" id="exampleInputEmail1" placeholder="Email">
                    <?php if(isset($data['email'])): ?>
                        <p><?php echo $data['email']; ?></p>
                    <?php endif;?>
                </div>
			</div>
		</div>

	</div>

	<div class="row-fluid center">
        <?php foreach(require_once(__DIR__.'/../../packages.php') as $package): ?>
		<div class="col-md-3 col-xs-12 col-sm-4 <?php echo $package['class'];  ?> ">
			<div class="row">
				<div class="pricing-table-header-<?php echo $package['class'];  ?>">
					<h2><?php echo $package['no_users'];  ?> Users</h2>
					<h3>Tiny</h3>
				</div>
				<div class="pricing-table-features">
					<p class="price">MVR<?php echo number_format($package['price'],2,'.',',');  ?></p>
				</div>
				<div class="pricing-table-signup-<?php echo $package['class'];  ?>">
					<p><button class="btn btn-primary" name="package" type="submit" value="<?php echo $package['no_users'];  ?>_user">Purchase</button></p>
				</div>
			</div>
		</div>
        <?php endforeach; ?>

		<input type="hidden" name="agreement" value="true"></input>
	</div>
</form>