<?php

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/16/16
 * Time: 4:21 PM
 */
class ICSPG
{


    /**
     * ICSPG constructor.
     */
    public function __construct()
    {
        $this->startListeningToBML();
        $this->register_post_type();
        $this->register_handlers();
    }

    public static function initialise()
    {
        new self();
    }


    /**
     *
     */
    private function register_post_type()
    {
        register_post_type('ics_sales',
            array(
                'labels' => array(
                    'name' => __( 'ICS Sales' ),
                    'singular_name' => __( 'ICSSale' )
                ),
                'public' => true,
                'capabilities' => array(
                    'create_posts' => 'edit_books',
                    'delete_post' => true,
                ),
                'has_archive' => false,
                'menu_icon' => plugins_url('resources/ics.ico',__FILE__),
                'show_ui'=>true,
                'rewrite' => array('slug' => 'ics_sales'),
            )
        );
    }


    private function register_handlers()
    {
        add_action('save_post', array($this, 'add_additional_fields'),10,2);
        add_action('admin_menu', array($this,'ics_sales_setting_menu'));
        add_filter("manage_edit-ics_sales_columns", array($this,"override_default_columns"));
        add_action("manage_ics_sales_posts_custom_column", array($this,"push_ics_sales_columns"), 10,2);
        add_shortcode( 'ics_sales', array($this,'package_list'));


        add_action('admin_init', array($this, 'setup_ics_settings'));

        wp_register_style( 'wpdocsPluginStylesheet', plugins_url( 'resources/bootstrap.min.css', __FILE__ ) );

        wp_enqueue_style( 'wpdocsPluginStylesheet' );
    }
    function package_list() {
        ob_start();
        $this->handleUI();
        return ob_get_clean();
    }


    function setup_ics_settings()
    {
        register_setting('ics_sales_options', 'ics_options', array($this, 'validate'));
    }

    function add_additional_fields($ics_sales_id, $ics_sales)
    {

        if ($ics_sales && $ics_sales->post_type == 'ics_sales' ) {
            if ( isset( $_POST['email'] ) && $_POST['email'] != '' ) {
                update_post_meta( $ics_sales_id, 'email', $_POST['email'] );
            }
            if ( isset( $_POST['package'] ) && $_POST['package'] != '' ) {
                update_post_meta( $ics_sales_id, 'package', $_POST['package'] );
            }
        }
    }

    /**
     *
     */
    function handleUI() {

        if (isset($_POST['agreement']) && $_POST['agreement'] )
        {
            $errors = array();
            if($_POST['email'] == $_POST['confirm_email'])
            {
                global $wpdb;

                $wp = $wpdb->get_var( "
                SELECT COUNT(*) FROM $wpdb->posts
                where post_type='ics_sales'
            " );

                $receipt_no = 'TRANS/'.$wp.'/'.date('Y');


                $packages = require_once 'packages.php';

                $package = $packages[$_POST['package']];

                require_once 'BMLSignature.php';

                $bml = new BMLSignature(
                    get_option('ics_pg_password'),
                    get_option('ics_pg_merchant_id'),
                    get_option('ics_pg_acquirer_id'),
                    $receipt_no,
                    $package['price'],
                    462
                );

                $post = wp_insert_post(
                    array(
                        'post_type'=>'ics_sales',
                        'post_title' => $receipt_no,
                        'post_status' => 'publish'
                    )
                );


                $this->view('resources.views.bml',false, (object)array('bml'=>$bml, 'package'=>$package));

            }
            else
            {
                $errors = array(
                    'email' => 'The emails do not match. Please correct them.'
                );
                $this->view('resources.views.form',false, $errors);
            }


            return;
        }
        else
            $this->view('resources.views.form');

    }

    function override_default_columns()
    {
        $columns = array(
            'cb'	 	=> '<input type="checkbox" />',
            'titles' 	=> 'Title',
            'package'	=>	'Package',
            'email'		=> 'Email',
            'verified'	=> 'Verified',
            'action'	=> 'Send License',
        );
        return $columns;
    }
    function push_ics_sales_columns($column)
    {
        global $post;
        switch ($column) {
            case 'titles':
                echo "<a href='#'>".$post->post_title."</a>";
                break;
            case 'email':
                echo "<a href='mailto:{$post->email}'>".$post->email."</a>";
                break;
            case 'status':
                echo $post->post_type;
                break;
            case 'package':
                echo $post->package;
                break;
            case 'verified':
                echo $post->verified ? 'Passed' : 'Failed';
                break;
            case 'action':
                if($post->verified) echo '<div class="wp-core-ui"><button class"button">Send</button></div>';
                break;
            default:
                # code...
                break;
        }
    }
    public function ics_sales_setting_menu()
    {
        	add_submenu_page('edit.php?post_type=ics_sales',
                'ICS Transactions Settings',
                'Settings',
                'manage_options',
                'setting_menu',
                array($this,'ics_sales_setting_view')
            );
    }

    public function ics_sales_setting_view()
    {
        $this->view("settings");
    }

    /**
     *
     */
    public function startListeningToBML()
    {
        add_action('parse_request', array($this, 'handle_bml_incoming'));
    }

    /**
     *
     */
    public function handle_bml_incoming()
    {
        if($_SERVER["REQUEST_URI"] == get_option('ics_pg_cb_url')) {


            if(isset($_POST['ResponseCode']))
            {
                switch($_POST['ResponseCode'])
                {
                    case 1:
                        $this->mark_as_verified();
                        $this->view('resources.views.success', true);
                        break;
                }
            }
            else
            {
                 $this->view('404');
            }
        }

    }

    private function parseId()
    {
        $re = "/(?!\\/)\\d+(?=\\/)/";
        $str = $_POST['OrderID'];
        preg_match($re, $str, $matches);

        return $matches[0];
    }

    private function mark_as_verified()
    {
        global $wpdb;
        $query = "
        SELECT * FROM wp_posts
        WHERE post_title = '{$_POST['OrderID']}'";

        $post = $wpdb->get_row($query,OBJECT, 0);


        update_post_meta($post->ID, 'verified', true );
    }
    
    
    private function view($view, $standAlone=false, $data=null){
        $file = str_replace('.','/', $view).'.php';
        if(file_exists(__DIR__.'/'.$file))
        {
            if($standAlone)
                get_header();

            include $file;

            if($standAlone)
                get_footer();

        }
        else
        {
            header("Status: 404 Not Found");
            global $wp_query;
            $wp_query->set_404();
            status_header(404);
            nocache_headers();
            //var_dump(getallheaders()); var_dump(headers_list()); die();
            header("location:/") or die();

        }

        exit;
    }
}