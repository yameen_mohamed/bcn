<?php

return array(
    '1_user' => array('price'=>12, 'no_users'=>1,'class'=>'tiny'),
    '2_user' => array('price'=>3000, 'no_users'=>2,'class'=>'small'),
    '3_user' => array('price'=>5000, 'no_users'=>3,'class'=>'medium'),
    '5_user' => array('price'=>8000, 'no_users'=>5,'class'=>'pro'),
);