
<div class="wrap">
    <h1>ICS PG Settings</h1>

    <form method="post" novalidate="novalidate">
        <br><font size="1"></font>
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row"><label for="password">Password</label></th>
                <td><input name="password" required="required" type="password" id="password" value="<?php echo get_option('ics_pg_password') ?>" class="regular-text"></td>
            </tr>
            <tr>
                <th scope="row"><label for="merchant_id">Merchant ID</label></th>
                <td><input name="merchant_id" required="required" type="text" id="merchant_id" aria-describedby="tagline-description"
                           value="<?php echo get_option('ics_pg_merchant_id') ?>" class="regular-text">

            </tr>
            <tr>
                <th scope="row"><label for="acquirer_id">Acquirer ID</label></th>
                <td><input name="acquirer_id" required="required" type="url" id="acquirer_id" value="<?php echo get_option('ics_pg_acquirer_id') ?>" class="regular-text code"></td>
            </tr>

            <tr>
                <th scope="row"><label for="cb_url">Callback URL</label></th>
                <td><input name="cb_url" required="required" type="url" id="cb_url" value="<?php echo get_option('ics_pg_cb_url') ?>" class="regular-text code">
                    <p>Make sure this is SSL. Note that it should be the relative path.</p>
                </td>
            </tr>

            </tbody>
        </table>
        <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p></form>
    </form>

</div>


<?php

if(isset($_POST['submit']))
{
    update_option('ics_pg_password',$_POST['password']);
    update_option('ics_pg_merchant_id',$_POST['merchant_id']);
    update_option('ics_pg_acquirer_id',$_POST['acquirer_id']);
    update_option('ics_pg_cb_url',$_POST['cb_url']);

}
