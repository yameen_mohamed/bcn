<?php
/*
Name: ICS Payment Gateway Integration
Plugin Name: ICS Payment Gateway Integration
Description: This facilitates the integration of BML payment gateway in sales of various products those ICS offers.
Version: 1.0
Author: Yameen Mohamed
Author URI: mailto:yaamynu@gmail.com

*/

require_once 'content/ICSPG.php';

add_action( 'init', function(){
     ICSPG::initialise();
});