<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/22/16
 * Time: 1:42 PM
 */

$group = wp_get_post_terms($post->ID,'page_group');

$is_grouped = count($group)>0;

$post_id = $post->ID;


$article = <<<EX


EX
;


$grouped_item_args = array(
    'post_type'=>'page',
    'tax_query' => array(
        array(
            'taxonomy' => 'page_group',
            'field' => 'term_id',
            'terms' => $group[0]->term_id,
        )
    )
);

$grouped_item_query = new WP_Query($grouped_item_args);

if($is_grouped):
?>

<div class="row">
    <div class="col-md-3">
        <!-- <h1>Rel</h1> -->
        <div class="list-group">
    <?php while($grouped_item_query->have_posts()):  $grouped_item_query->the_post() ?>
        <a href="<?php the_permalink(); ?>" class="list-group-item <?php if($post_id == $post->ID) echo 'active'; ?>"><?php the_title(); ?></a>
    <?php endwhile; wp_reset_postdata();?>

        </div>
    </div>
    <div class="col-md-9">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </header><!-- .entry-header -->

            <div class="entry-content">
                <?php the_content(); ?>
                <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
            </div><!-- .entry-content -->
            <footer class="entry-meta">
                <?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
            </footer><!-- .entry-meta -->
        </article>
    </div>
</div>

<?php else: ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">
            <h1 class="entry-title"><?php the_title(); ?></h1>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <?php the_content(); ?>
            <?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
        </div><!-- .entry-content -->
        <footer class="entry-meta">
            <?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>
        </footer><!-- .entry-meta -->
    </article>
<?php endif; ?>




