<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/17/16
 * Time: 2:04 PM
 */


?>
<div id="comments">

    <?php if ( have_comments() ) : ?>
        <h2 id="comments-title">
            <?php
            printf( _n( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'twentyeleven' ),
                number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
            ?>
        </h2>

        <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
            <nav id="comment-nav-above">
                <h1 class="assistive-text">'Comment navigation'></h1>
                <div class="nav-previous"><?php previous_comments_link( '&larr; Older Comments' ); ?></div>
                <div class="nav-next"><?php next_comments_link( 'Newer Comments &rarr;' ); ?></div>
            </nav>
        <?php endif; // check for comment navigation ?>

        <ol class="commentlist">
            <?php
            require_once('class-wp-bootstrap-comment-walker.php');

            wp_list_comments( array(
                'style'         => 'ul',
                'short_ping'    => true,
                'avatar_size'   => '64',
                'walker'        => new Bootstrap_Comment_Walker(),
            ) );
            ?>
        </ol>

        <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
            <nav id="comment-nav-below">
                <h1 class="assistive-text"><?php _e( 'Comment navigation', 'twentyeleven' ); ?></h1>
                <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'twentyeleven' ) ); ?></div>
                <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'twentyeleven' ) ); ?></div>
            </nav>
            <a href="#" data-toggle="modal" data-target="#myModal">Comments?</a>
            <hr>
        <?php endif;

        endif;
        // check for comment navigation ?>






        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Your opinion</h4>
                    </div>
                    <div class="modal-body">
                        <?php

                        $comments_args = array(
                            'fields'=>array(
                                'email'=>'<div class="form-group"><label>Email</label><input class="form-control" type="email" required name="email" id="email" placeholder="Your email" /></div>',
                                'author'=>'<div class="form-group"><label>Name</label><input class="form-control" type="text" required name="author" id="author" placeholder="Your name" /></div>',
                            ),
                            // change the title of send button
                            'label_submit'=>'Proceed',
                            'class_submit' => 'hide',
                            // change the title of the reply section
                            'title_reply'=> '<div/>',
                            'id_form'=>'comment_form',
                            'comment_notes_before'=>'We value your opinion and your privacy.',
                            // remove "Text or HTML to be displayed after the set of comment fields"
                            'comment_notes_after' => '',
                            // redefine your own textarea (the comment body)
                            'comment_field' =>  '<div class="form-group"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><textarea id="comment" class="form-control" rows="5" placeholder="Your opinion which we value." name="comment" aria-required="true"></textarea></div>',
                        );

                        comment_form($comments_args);

                        ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" form="comment_form" class="btn btn-success">Proceed</button>
                    </div>
                </div>
            </div>
        </div>

</div>

