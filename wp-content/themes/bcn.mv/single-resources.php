<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/28/16
 * Time: 11:06 AM
 */
require_once ('wp_bootstrap_walker.php');
get_header();
ob_start();
dynamic_sidebar( 'primary' ); // or whatever the sidebar-area is called.
$sidebar = ob_get_clean();
?>


<div class="container">
    <div id="content" role="main">
        <?php if(has_post_thumbnail()) : ?>
            <img width="100%" src="<?php the_post_thumbnail_url(); ?>" alt="">
        <?php endif;?>
        
        <h1 class="entry-title"><?php the_title(); ?></h1>
        <div class="row">
            <div class="col-md-8">
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <div class="entry-meta">
            <!--            --><?php //twentyten_posted_on(); ?>
                    </div><!-- .entry-meta -->

                    <div class="entry-content ">
                        <?php if($post->post_excerpt): ?>
                        <blockquote class="quote">
                            <?php the_excerpt() ?>
                        </blockquote>
                        <?php endif; ?>
                        <?php echo $post->post_content; ?>
                        <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:' ), 'after' => '</div>' ) ); ?>

                    </div><!-- .entry-content -->
                    <hr>
                    <p><i>Click to download</i></p>
                    <a href="<?php pdf_file_url(); ?>"> <img class="icon" src="<?php echo get_template_directory_uri() . '/resources/images/pdf.png' ?>"  /> <?php the_title() ?></a>
                </div>
            </div>
            <div class="col-md-4 side_bar">
                <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:' ), 'after' => '</div>' ) ); ?>
            </div>
        </div>
    </div><!-- #content -->
</div><!-- #container -->

<?php get_footer(); ?>
