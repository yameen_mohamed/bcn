        </div>
    </div>
    <footer class="footer nav navbar-default">
        <div class="container padding-50">
            <nav>
                <?php

                $menus = get_terms( 'nav_menu', array( 'hide_empty' => true ) );


                foreach ( $menus as $menu ):

                    ?>
                    <ul class="menu-class nav navbar-nav">
                        <?php wp_nav_menu(array(
                            'menu' => $menu,
                            'walker'=> new wp_bootstrap_navwalker(),
                            'menu_class'        => 'nav',
                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',

                        ));?>
                    </ul>

                <?php endforeach; wp_footer();?>

            </nav>
        </div>
        <hr>
        <div class="container  padding-50">
            <p>All rights reserved by MED</p>
        </div>

    </footer>
<script src="<?php echo get_template_directory_uri(); ?>/resources/app.js"></script>
</body>
</html>