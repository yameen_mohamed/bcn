<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/22/16
 * Time: 1:59 PM
 */


?>
<div class="container">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <h1 class="entry-title"><?php the_title(); ?></h1>

        <div class="entry-meta">
<!--            --><?php //twentyten_posted_on(); ?>
        </div><!-- .entry-meta -->

        <div class="entry-content ">
            <?php if($post->post_excerpt): ?>
            <blockquote class="quote">
                <?php the_excerpt() ?>
            </blockquote>
            <?php endif; ?>
            <?php the_content(); ?>
            <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:' ), 'after' => '</div>' ) ); ?>

        </div><!-- .entry-content -->


        <div class="entry-utility">
<!--            --><?php //twentyten_posted_in(); ?>
            <?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
        </div><!-- .entry-utility -->
    </div><!-- #post-## -->



    <div id="nav-below clearfix" class="navigation">
        <div class="nav-previous btn">
            <?php previous_post_link( '%link', '%title' ); ?>
        </div>
        <div class="nav-next btn"><?php next_post_link( '%link', '%title' ); ?></div>
    </div><!-- #nav-below -->
    <hr>

    <?php comments_template( '', true ); ?>

<?php endwhile; // end of the loop. ?>

</div>


