<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/17/16
 * Time: 2:57 AM
 */


date_default_timezone_set('Indian/Maldives');

require_once ('resources/libs/slider/Slider.php');
require_once ('resources/libs/youtube/YouTube.php');
require_once ('resources/libs/resources/Resource.php');
require_once ('resources/libs/services/Service.php');
require_once ('resources/libs/products/Products.php');
require_once ('resources/libs/trainings/Training.php');
require_once 'resources/libs/page/Page.php';
require_once 'resources/widgets/services/services.php';
require_once 'resources/widgets/trainings/trainings.php';




function bcn_login() {
    echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/styles.css" />';
}
add_action('login_head', 'bcn_login');


function wptutsplus_register_theme_menu() {
    new Slider();
    new YouTube();
    new Resource();
    new Service();
    new Product();
    new Page(); new Training();
    register_nav_menu( 'primary', 'Main Navigation Menu' );
    register_nav_menu( 'tabs', 'Tabs On the slider' );

}

define('WP_ADMIN_DIR', 'admin-panel');
define( 'ADMIN_COOKIE_PATH', SITECOOKIEPATH . WP_ADMIN_DIR);

//add_action('login_form','redirect_wp_admin');

function redirect_wp_admin(){
    $redirect_to = $_SERVER['REQUEST_URI'];

    if(count($_REQUEST)> 0 && array_key_exists('redirect_to', $_REQUEST)){
        $redirect_to = $_REQUEST['redirect_to'];
        $check_wp_admin = stristr($redirect_to, 'wp-admin');
        if($check_wp_admin){
            wp_safe_redirect( '/house' );
        }
    }
}



add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

function remove_wp_logo( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'wp-logo' );

}

add_action('customize_register', 'themename_customize_register');
function themename_customize_register($wp_customize) {
    $wp_customize->remove_section( 'static_front_page' );

}

add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );

add_action( 'init', 'wptutsplus_register_theme_menu' );


register_sidebar(array(
    'name'=> 'Top Tabs',
    'id' => 'top_tabs',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h2 class="offscreen">',
    'after_title' => '</h2>',
));

register_sidebar(array(
    'name'=> 'Service Tabs',
    'id' => 'service_tabs',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h2 class="offscreen">',
    'after_title' => '</h2>',
));


//add_filter( 'comment_form_default_fields', 'bootstrap3_comment_form_fields' );
//function bootstrap3_comment_form_fields( $fields ) {
//    $commenter = wp_get_current_commenter();
//
//    $req      = get_option( 'require_name_email' );
//    $aria_req = ( $req ? " aria-required='true'" : '' );
//    $html5    = current_theme_supports( 'html5', 'comment-form' ) ? 1 : 0;
//
//    $fields   =  array(
//        'author' => '<div class="form-group comment-form-author">' . '<label for="author">' . __( 'Name' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
//            '<input class="form-control" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></div>',
//        'email'  => '<div class="form-group comment-form-email"><label for="email">' . __( 'Email' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
//            '<input class="form-control" id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></div>',
////        'url'    => '<div class="form-group comment-form-url"><label for="url">' . __( 'Website' ) . '</label> ' .
////            '<input class="form-control" id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></div>'
//    );
//
//    return $fields;
//}


add_filter( 'comment_form_defaults', 'bootstrap3_comment_form' );
function bootstrap3_comment_form( $args ) {
    $args['comment_field'] = '<div class="form-group comment-form-comment">
            <label for="comment">' . _x( 'Comment', 'noun' ) . '</label>
            <textarea class="form-control" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
        </div>';
    $args['class_submit'] = 'btn btn-default'; // since WP 4.1

    return $args;
}

//add_action('comment_form', 'bootstrap3_comment_button' );
//function bootstrap3_comment_button() {
//    echo '<button class="btn btn-default" type="submit">' . __( 'Submit' ) . '</button>';
//}


function custom_theme_setup() {
    add_theme_support( 'html5', array( 'comment-list' ) );
}
add_action( 'after_setup_theme', 'custom_theme_setup' );


/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function setDashBoard() {

    wp_add_dashboard_widget(
                 'bcn_dashboard',           // Widget slug.
                 'BCN Info',                // Title.
                 'bcn_dashboard_function'   // Display function.
        );  
}
add_action( 'wp_dashboard_setup', 'setDashBoard' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function bcn_dashboard_function() {

    // Display whatever it is you want to show.
    echo "Hello World, I'm a great Dashboard Widget";
}





