<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/22/16
 * Time: 12:31 PM
 */
require_once ('wp_bootstrap_walker.php');
get_header();
ob_start();
dynamic_sidebar( 'primary' ); // or whatever the sidebar-area is called.
$sidebar = ob_get_clean();
?>


<div class="container">
    <div id="content" role="main">
        <?php if(has_post_thumbnail()) : ?>
        <img width="100%" src="<?php the_post_thumbnail_url(); ?>" alt="">
        <?php endif;?>
        <?php
        /*
         * Run the loop to output the post.
         * If you want to overload this in a child theme then include a file
         * called loop-single.php and that will be used instead.
         */
        get_template_part( 'loop', 'single' );
        ?>

    </div><!-- #content -->
</div><!-- #container -->

<?php get_footer(); ?>
