<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/29/16
 * Time: 12:17 PM
 */
require_once ('wp_bootstrap_walker.php');
require_once ('BootstrapTabsWalker.php');
get_header();
ob_start();
dynamic_sidebar( 'primary' ); // or whatever the sidebar-area is called.
$sidebar = ob_get_clean();
?>


<img src="<?php echo get_template_directory_uri() . '/resources/widgets/media/resources/MediaCentre.jpg' ?>" alt="">
<div class="container">
    
    <h2>Media Center</h2>

    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

            <div class="entry-meta">
                <!--            --><?php //twentyten_posted_on(); ?>
            </div><!-- .entry-meta -->

            <div class="entry-content">
                <?php the_excerpt(); ?>
                <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
            </div><!-- .entry-content -->



            <div class="entry-utility">
                <!--            --><?php //twentyten_posted_in(); ?>
                <?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
            </div><!-- .entry-utility -->
        </div><!-- #post-## -->




    <?php endwhile; // end of the loop. ?>

</div>

<?php get_footer(); ?>
