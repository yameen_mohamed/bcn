/* File: gulpfile.js */

// grab our gulp packages
var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify-css')

;

// create a default task and just log a message
gulp.task('default', function() {
    gulp.src([
        './bower_components/jquery/dist/jquery.js',
        './bower_components/bootstrap/dist/js/bootstrap.js',
    ])
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('resources'));

    gulp.src([
        './bower_components/bootstrap/dist/css/bootstrap.css',
        './bower_components/bootstrap-flat/css/bootstrap-flat.css',
    ])
        .pipe(concat('app.css'))
        .pipe(minify())
        .pipe(gulp.dest('resources'))

});