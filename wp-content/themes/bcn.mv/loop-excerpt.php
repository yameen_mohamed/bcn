<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/28/16
 * Time: 12:16 PM
 */

?>
<div class="container">

    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

            <div class="entry-meta">
                <!--            --><?php //twentyten_posted_on(); ?>
            </div><!-- .entry-meta -->

            <div class="entry-content">
                <?php the_excerpt(); ?>
                <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
            </div><!-- .entry-content -->



            <div class="entry-utility">
                <!--            --><?php //twentyten_posted_in(); ?>
                <?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
            </div><!-- .entry-utility -->
        </div><!-- #post-## -->




    <?php endwhile; // end of the loop. ?>

</div>