<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/22/16
 * Time: 1:59 PM
 */

require_once ('wp_bootstrap_walker.php');
get_header();
ob_start();
dynamic_sidebar( 'primary' ); // or whatever the sidebar-area is called.
$sidebar = ob_get_clean();
?>
<div class="container">
    <h2>We do not have what you are looking for!</h2>
    <h4>Let us know!</h4>
    <ol>
        <li>Facebook <a href="mailto:info@bcn.mv">info@bcn.mv</a></li>
        <li>Twitter <a href="mailto:info@bcn.mv">info@bcn.mv</a></li>
        <li>Email us on <a href="mailto:info@bcn.mv">info@bcn.mv</a></li>
    </ol>
    <h4>Thank you!</h4>
</div>

<?php get_footer() ?>