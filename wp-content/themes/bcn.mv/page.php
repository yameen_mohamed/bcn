<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/22/16
 * Time: 12:32 PM
 * Template Name: Full Width Page
 */


require_once ('wp_bootstrap_walker.php');
get_header();
ob_start();
dynamic_sidebar( 'primary' ); // or whatever the sidebar-area is called.
$sidebar = ob_get_clean();
?>
<?php if(has_post_thumbnail()) : ?>
<div class="row">
    <div class="container-fluid">
        <img width="100%" src="<?php the_post_thumbnail_url(); ?>" alt="xxx">
    </div>
</div>
<?php endif; ?>
<div class="container">
    <div class="col-md-10">
        <div id="primary" class="site-content">
            <div id="content" role="main">

                <?php while ( have_posts() ) : the_post(); ?>
                    <?php get_template_part( 'content', 'page' ); ?>
                    <!--                --><?php //comments_template( '', true ); ?>
                <?php endwhile; // end of the loop. ?>

            </div><!-- #content -->
        </div><!-- #primary -->
    </div>
    <div class="col-md-2">
        <?php get_sidebar(); ?>
    </div>
</div>




<?php get_footer(); ?>

