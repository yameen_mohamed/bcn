<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/28/16
 * Time: 11:06 AM
 */
require_once ('wp_bootstrap_walker.php');
get_header();
ob_start();
dynamic_sidebar( 'primary' ); // or whatever the sidebar-area is called.
$sidebar = ob_get_clean();
?>


<div class="container">
    <div id="content" role="main">
        <?php if(has_post_thumbnail()) : ?>
            <img width="100%" src="<?php the_post_thumbnail_url(); ?>" alt="">
        <?php endif;?>

        <h1 class="entry-title"><?php the_title(); ?></h1>
        <div class="row">
            <div class="col-md-8">
                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <div class="entry-meta">
                        <!--            --><?php //twentyten_posted_on(); ?>
                    </div><!-- .entry-meta -->

                    <div class="entry-content ">
                        <?php if($post->post_excerpt): ?>
                            <blockquote class="quote">
                                <?php the_excerpt() ?>
                            </blockquote>
                        <?php endif; ?>

                        <?php the_content(); ?>

                        <?php echo ($post->post_content); ?>

                        <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:' ), 'after' => '</div>' ) ); ?>

                    </div><!-- .entry-content -->
                </div>
            </div>
            <div class="col-md-4 side_bar">
                <h3>Training Details</h3>
                <p>Starting: <?php echo get_the_training_start_date(); ?></p>
                <p>Ending: <?php echo get_the_training_end_date(); ?></p>
                <p>At: <strong><?php echo get_the_venues_list(); ?></strong> </p>
                <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:' ), 'after' => '</div>' ) ); ?>
            </div>
        </div>
    </div><!-- #content -->
    <br>
</div><!-- #container -->

<?php get_footer(); ?>
