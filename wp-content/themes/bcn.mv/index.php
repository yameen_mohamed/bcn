<?php

require_once ('wp_bootstrap_walker.php');
require_once ('BootstrapTabsWalker.php');
get_header();
ob_start();
dynamic_sidebar( 'primary' ); // or whatever the sidebar-area is called.
$sidebar = ob_get_clean();
?>

<?php if (is_home() && function_exists('dynamic_sidebar') && dynamic_sidebar('top_tabs')) : else : ?>
    <div class="pre-widget">
    </div>
<?php endif; ?>

    <?php if(is_home()): ?>
        <div class="container-fluid box">
            <div class="container">
                <h2>Our products</h2>
                <h5>Our Service has just got better!</h5>

                <ul class="iconic">
                    <?php
                        $products_args = array(
                            'post_type'=>'products',
                            'posts_per_page'=>4
                        );

                        $products_query  = new WP_Query($products_args);
                        while($products_query->have_posts()): $products_query->the_post();
                    ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <div class="soi-icon ra-icon">
                                <img width="100%" src="<?php the_post_thumbnail_url() ?>" alt="">
                            </div>
                            <div class="special"><h5><?php the_title(); ?></h5></div>
                           <!--  <p>
                                <?php the_excerpt(); ?>
                                </p> -->
                            </a>
                    </li>

                    <?php endwhile;
                    $product_link = get_post_type_archive_link('products');

                    ?>
                </ul>


                <a class="more" href="<?php echo esc_url( $product_link ); ?>" title="Products">More</a>

            </div>
        </div>
        <div class="container-fluid box grey">
            <div class="container padding-20">
                <h2>Media</h2>
                <div class="row">
                    <?php

                        $mediaReleases = new WP_Query(array(
                           'category_name'=>'media-releases'
                        ));

                        if($mediaReleases->have_posts()):

                            while($mediaReleases->have_posts()): $mediaReleases->the_post();
                    ?>

                                <div class="col-md-3">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail(); ?>
                                        <?php the_title(); ?>
                                    </a>
                                <p class="date"><?php the_modified_date() ?></p>
                                </div>

                    <?php
                            endwhile;
                        endif;

                    ?>

                    <?php
                    // Get the ID of a given category
                    $category_id = get_cat_ID( 'Media Releases' );

//                    die($category_id);

                    // Get the URL of this category
                    $category_link = get_category_link( $category_id );
                    ?>

                    <br>
                    <!-- Print a link to this category -->
                    <a class="more" href="<?php echo esc_url( $category_link ); ?>" title="Media">More</a>
                </div>
<!--                <div class="card">-->
<!--                    <h2 class="">Public Declarations</h2>-->
<!--                    <ol class="news-list">-->
<!--                        --><?php
//
//                        $publicDeclaration = new WP_Query(array(
//                            'category_name'=>'public-declaration'
//                        ));
//
//                        if($publicDeclaration->have_posts()):
//
//                            while($publicDeclaration->have_posts()): $publicDeclaration->the_post();
//                                ?>
<!---->
<!--                                <li><a href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!--                                    <p class="date">--><?php //the_modified_date() ?><!--</p>-->
<!--                                </li>-->
<!---->
<!--                                --><?php
//                            endwhile;
//                        endif;
//
//                        ?>
<!--                    </ol>-->
<!--                </div>-->
            </div>
        </div>
        <div class="container-fluid box">
            <div class="container padding-20">
                <h2>Videos</h2>
                <div class="row">

                <?php

                $mediaReleases = new WP_Query(array(
                    'post_type'=>'videos'
                ));

                if($mediaReleases->have_posts()):

                    while($mediaReleases->have_posts()): $mediaReleases->the_post();
                        ?>

                            <div class="col-md-3 padding-20 frame">
                                <?php echo $post->embed_code; ?>
                            <p>
                                <h4>
                                    <?php the_title(); ?>
                                </h4>
                            </p>
                            </div>


                        <?php
                    endwhile;
                endif;

                ?>
                </div>
                <a class="more" href="<?php echo esc_url( get_post_type_archive_link('videos') ); ?>" title="Videos">More</a>

            </div>
        </div>
        <div class="container-fluid box grey">
            <div class="container padding-20">
                <h2>Resources</h2>
                <div class="row">
                    <div class="col-md-4">
                        <h5>Downloads</h5>
                        <?php
                        $downloads = new WP_Query(array(
                            'post_type'=>'resources',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'resources_categories',
                                    'field'    => 'slug',
                                    'terms'    => 'downloads',
                                ),
                            ),
                        ));

                        if($downloads->have_posts()):

                            while($downloads->have_posts()): $downloads->the_post();
                                ?>

                                <div class="frame">
                                    <p>
                                    <a href="<?php the_permalink() ?>">
                                        <?php the_title(); ?>
                                    </a>
                                    </p>
                                </div>


                                <?php
                            endwhile;
                        endif;
                        ?>


                    <?php
                    $resources_link = get_term_link( 'downloads', 'resources_categories' );
                    ?>
                        <a class="more" href="<?php echo esc_url( $resources_link ); ?>" title="Downloads">More Downloads</a>

                    </div>

                    <div class="col-md-4">
                        <h5>Laws</h5>
                        <?php
                        $mediaReleases = new WP_Query(array(
                            'post_type'=>'resources',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'resources_categories',
                                    'field'    => 'slug',
                                    'terms'    => 'laws',
                                ),
                            ),
                        ));

                        if($mediaReleases->have_posts()):

                            while($mediaReleases->have_posts()): $mediaReleases->the_post();
                                ?>

                                <div class="frame">
                                    <p>
                                        <a href="<?php the_permalink() ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </p>
                                </div>


                                <?php
                            endwhile;
                        endif;
                        $laws_link = get_term_link( 'laws', 'resources_categories' );

                        ?>

                        <a class="more" href="<?php echo esc_url( $laws_link ); ?>" title="Media">More Laws</a>

                    </div>

                </div>

            </div>
        </div>
        <div class="container-fluid box">
        <div class="container padding-20">
        <h2>Tools</h2>
            <ul class="iconic">
                <li>
                    <a href="/onlinesolutions/Pages/default.aspx/retirement.html">
                        <div class="soi-icon ra-icon">
                            <img width="40" src="<?php echo  get_template_directory_uri(); ?>/resources/images/calculator.png" alt="">
                        </div>
                        <h4>Loan Calculator</h4>
                </li>

            </ul>
        </div>
    </div>
    <?php else : ?>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <?php if (have_posts()): ?>
                        <?php while (have_posts()) : the_post(); ?>
                            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <div class="post-header">
                                    <h2><a href="<?php the_permalink(); ?>" rel="bookmark"
                                           title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                                    </h2>

                                    <div class="">
                                        <div class="date-box">
                                            <span class="date-box-month"><?php the_time('M'); ?></span><span
                                                class="date-box-day"><?php the_time('d'); ?></span><span
                                                class="date-box-year"><?php the_time('Y'); ?></span>
                                        </div>
                                        <div class="padding">
                                            <?php the_excerpt(); ?>
                                        </div>
                                    </div>


                                    <div class="author">
                                        <?php //the_author(); ?>
                                    </div>
                                </div>
                                <!--end post header-->
                                <div class="entry clear">
<!--                                    --><?php //if (function_exists('add_theme_support')) the_post_thumbnail(); ?>
                                    <?php edit_post_link(); ?>
                                    <?php wp_link_pages(); ?>
                                </div>
                                <!--end entry-->
                                <div class="post-footer">
                                    <div
                                        class="comments"><?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?></div>
                                </div>
                                <!--end post footer-->
                            </div><!--end post-->
                        <?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
                        <div class="navigation index">
                            <div class="alignleft"><?php next_posts_link('Older Entries'); ?></div>
                            <div class="alignright"><?php previous_posts_link('Newer Entries'); ?></div>
                        </div><!--end navigation-->
                    <?php else: ?>
                    <?php endif; ?>
                    <br>

                    <div class="comment list">
                        <?php comments_template(); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

<?php get_footer(); ?>






