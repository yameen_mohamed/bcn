<div class="align-center">

<?php
global $wp_locale;
$month = get_post_meta($post->ID, $metabox_id . '_month', true);

if (empty($month)) {
    $month = date('m');
}
?>
<select name=" <?php echo $metabox_id . '_month' ?>" value="<?php echo $month; ?>">
    <?php

    for ($i = 1; $i < 13; $i++) {

        $month_s = '<option value="' . zeroise($i, 2) . '"';
        if ($i == $month)
            $month_s .= ' selected="selected"';
        $month_s .= '>' . $wp_locale->get_month_abbrev($wp_locale->get_month($i)) . "</option>";
        echo $month_s;
    }


    ?>
</select>
<select name=" <?php echo $metabox_id . '_day' ?>" value="<?php echo $day; ?>">
    <?php
    $day = get_post_meta($post->ID, $metabox_id . '_day', true);

    if (empty($day)) {
        $day = date('d');
    }

    for ($i = 1; $i < 32; $i++) {

        $month_s = '<option value="' . zeroise($i, 2) . '"';
        if ($i == $day)
            $month_s .= ' selected="selected"';
        $month_s .= '>' . zeroise($i,2) . "</option>";
        echo $month_s;
    }

    ?>
</select>
<select name=" <?php echo $metabox_id . '_year' ?>" value="<?php echo $year; ?>">
    <?php
    $year = get_post_meta($post->ID, $metabox_id . '_year', true);

    if (empty($year)) {
        $year = date('Y');
    }

    for ($i = $year-2; $i < $year+2; $i++) {

        $month_s = '<option value="' . zeroise($i, 2) . '"';
        if ($i == $year)
            $month_s .= ' selected="selected"';
        $month_s .= '>' . zeroise($i,2) . "</option>";
        echo $month_s;
    }

    ?>
</select>
<br>

<select name=" <?php echo $metabox_id . '_hour' ?>" value="<?php echo $hour; ?>">
    <?php
    $hour = get_post_meta($post->ID, $metabox_id . '_hour', true);

    if (empty($hour)) {
        $hour = date('H');
    }


    for ($i = 0; $i < 24; $i++) {

        $month_s = '<option value="' . zeroise($i, 2) . '"';
        if ($i == $hour)
            $month_s .= ' selected="selected"';
        $month_s .= '>' . zeroise($i,2) . "</option>";
        echo $month_s;
    }

    ?>
</select> :
<select name=" <?php echo $metabox_id . '_minute' ?>" value="<?php echo $minute; ?>">
    <?php
    $minute = get_post_meta($post->ID, $metabox_id . '_minute', true);

    if (empty($minute)) {
        $minute = date('i');
    }

    for ($i = 0; $i < 60; $i++) {

        $month_s = '<option value="' . zeroise($i, 2) . '"';
        if ($i == $minute)
            $month_s .= ' selected="selected"';
        $month_s .= '>' . zeroise($i,2) . "</option>";
        echo $month_s;
    }

    ?>
</select>
<?php

//echo '<input type="text" name="' . $metabox_id . '_day" value="' . $day . '" size="2" maxlength="2" />';
//echo '<input type="text" name="' . $metabox_id . '_year" value="' . $year . '" size="4" maxlength="4" /> @ ';
//echo '<input type="text" name="' . $metabox_id . '_hour" value="' . $hour . '" size="2" maxlength="2"/>:';
//echo '<input type="text" name="' . $metabox_id . '_minute" value="' . $min . '" size="2" maxlength="2" />';
?>

</div>


