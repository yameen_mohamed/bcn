<?php

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 3/29/16
 * Time: 12:39 PM
 */
class Training
{
    /**
     * YouTube constructor.
     */
    public function __construct()
    {
        $this->register_handlers();
        add_action('admin_menu', array($this, 'create_meta_box_video'));
        $this->products_taxonomy();


    }


    function products_taxonomy() {
        register_taxonomy(
            'training_venues',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'trainings',        //post type name
            array(
                'hierarchical' => false,
                'label' => 'Venues',  //Display name
                'labels' => array(
                    'name'                           => 'Venues',
                    'singular_name'                  => 'Venue',
                    'search_items'                   => 'Search Venues',
                    'all_items'                      => 'All Venues',
                    'edit_item'                      => 'Edit Venue',
                    'update_item'                    => 'Update Venue',
                    'add_new_item'                   => 'Add New Venue',
                    'new_item_name'                  => 'New Venue Name',
                    'menu_name'                      => 'Venue',
                    'view_item'                      => 'View Venue',
                    'popular_items'                  => 'Popular Venue',
                    'separate_items_with_commas'     => 'Separate venues with commas',
                    'add_or_remove_items'            => 'Add or remove venues',
                    'choose_from_most_used'          => 'Choose from the most used venues',
                    'not_found'                      => 'No venues found'
                ),
                'public' => true,
                'query_var' => true,
                'rewrite' => array(
                    'slug' => 'training_venue', // This controls the base slug that will display before each term
                    'with_front' => false, // Don't display the category base before
                )
            )
        );
    }

    private function register_handlers()
    {
        register_post_type('trainings',
            array(
                'labels' => array(
                    'name' => __( 'Trainings' ),
                    'singular_name' => __( 'Training' ),
                    'add_new_item' => 'Add New Training'
                ),
                'public' => true,
                'taxonomies' => array('post_tag'),
                'supports' => array('title','media' ,'editor','excerpt'),
                'has_archive' => true,
                'menu_icon' => get_template_directory_uri().'/resources/libs/trainings/resources/icon.png',
                'show_ui'=>true,
                'rewrite' => array('slug' => 'trainings'),
            )
        );
//        add_action('post_edit_form_tag', array($this,'add_form_file_support'));
        add_action('save_post', array($this,'save'), 12);

        add_filter("manage_edit-trainings_columns", array($this,"override_default_columns"));
        add_action("manage_trainings_posts_custom_column", array($this,"push_trainings_columns"), 10,2);

    }

    function override_default_columns()
    {
        $columns = array(
            'cb'	 	=> '<input type="checkbox" />',
            'title' 	=> 'Title',
            'start'	=>	'Start',
            'end'	=>	'End',
            'venue' => 'Venue'
        );
        return $columns;
    }
    function push_trainings_columns($column)
    {
        global $post;
        switch ($column) {
            case 'image':
                echo $post->embed_code;
                break;
            case 'start':
                $this->the_training_start_date(); break;
            case 'end':
                $this->the_training_end_date(); break;
            case 'venue':
                $this->the_venues_list(); break;
        }
    }

    function create_meta_box_video()
    {
        add_meta_box( 'meta_start', 'Start Time', array($this,'new_meta_boxes_video'), 'trainings', 'side', 'high' );
        add_meta_box( 'meta_end', 'End Time', array($this,'new_meta_boxes_video'), 'trainings', 'side', 'high' );
    }
    function save($id){
        global $post;
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return;



        // Is the user allowed to edit the post or page?
        if ( !current_user_can( 'edit_post', $post->ID ) )
            return;
        $metas = array('meta_start_','meta_end_');
        $fields = array('year','month','hour','minute','day');

        if($post->post_type == 'trainings')
        {
            foreach($metas  as $meta)
            {
                foreach ($fields as $field) {
                    $str = $meta . $field;
                    if(isset($_POST[$str]))
                        update_post_meta($id, $str , $_POST[$str]);
                }
            }
        }
    }

    function the_training_start_date() {
        global $post;
        $month = get_post_meta($post->ID, 'meta_start_month', true);
        $trainingDate = getMonthAbbr($month);
        $trainingDate .= ' ' . get_post_meta($post->ID, 'meta_start_day', true) . ',';
        $trainingDate .= ' ' . get_post_meta($post->ID, 'meta_start_year', true);
        $trainingDate .= ' at ' . get_post_meta($post->ID, 'meta_start_hour', true);
        $trainingDate .= ':' . get_post_meta($post->ID, 'meta_start_minute', true);
        echo $trainingDate;
    }

    function the_training_end_date() {
        global $post;
        $month = get_post_meta($post->ID, 'meta_end_month', true);
        $trainingDate = getMonthAbbr($month);
        $trainingDate .= ' ' . get_post_meta($post->ID, 'meta_end_day', true) . ',';
        $trainingDate .= ' ' . get_post_meta($post->ID, 'meta_end_year', true);
        $trainingDate .= ' at ' . get_post_meta($post->ID, 'meta_end_hour', true);
        $trainingDate .= ':' . get_post_meta($post->ID, 'meta_end_minute', true);
        echo $trainingDate;
    }


    function the_venues_list()
    {
        echo implode(', ', array_map(function($item){
            return $item->name;
        }, get_the_terms(get_the_ID(), 'training_venues')));
    }



    function new_meta_boxes_video($post, $args) {
        $metabox_id = $args['id'];
        global $post;
        $meta_box_value = get_post_meta($post->ID,'embed_code',true);
        if($meta_box_value) { echo html_entity_decode($post->embed_code); }

        include 'views/time.php';
    }

}

function getMonthAbbr($month) {
    global $wp_locale;
    for ( $i = 1; $i < 13; $i = $i +1 ) {
        if ( $i == $month )
            $monthabbr = $wp_locale->get_month_abbrev( $wp_locale->get_month( $i ) );
    }
    return $monthabbr;
}

function get_the_training_start_date() {
    global $post;
    $month = get_post_meta($post->ID, 'meta_start_month', true);
    $trainingDate = getMonthAbbr($month);
    $trainingDate .= ' ' . get_post_meta($post->ID, 'meta_start_day', true) . ',';
    $trainingDate .= ' ' . get_post_meta($post->ID, 'meta_start_year', true);
    $trainingDate .= ' at ' . get_post_meta($post->ID, 'meta_start_hour', true);
    $trainingDate .= ':' . get_post_meta($post->ID, 'meta_start_minute', true);
    return $trainingDate;
}

function get_the_training_end_date() {
    global $post;
    $month = get_post_meta($post->ID, 'meta_end_month', true);
    $trainingDate = getMonthAbbr($month);
    $trainingDate .= ' ' . get_post_meta($post->ID, 'meta_end_day', true) . ',';
    $trainingDate .= ' ' . get_post_meta($post->ID, 'meta_end_year', true);
    $trainingDate .= ' at ' . get_post_meta($post->ID, 'meta_end_hour', true);
    $trainingDate .= ':' . get_post_meta($post->ID, 'meta_end_minute', true);
    return $trainingDate;
}


function get_the_venues_list()
{
    return implode(', ', array_map(function($item){
        return $item->name;
    }, get_the_terms(get_the_ID(), 'training_venues')));
}