<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/18/16
 * Time: 11:56 AM
 */

?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/bower_components/Swiper/dist/css/swiper.min.css">
<style>
    html, body {
        position: relative;
        height: 100%;
    }

    body {
        font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
        font-size: 14px;
        color: #000;
        margin: 0;
        padding: 0;
    }

    nav.navbar.navbar-default {
        z-index: 10;
        background-color: transparent;
        border-color: transparent;
    }
    .navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus, .navbar-default .navbar-nav>.active>a:hover {
        color: #FFF;
        background-color: rgba(231,231,231,0.5);
    }
    .navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
        color: #FFF;
        background-color: transparent;
    }
    .navbar-default .navbar-nav>li>a, .navbar-default .navbar-text {
        color: #FFF;
    }

    .swiper-container {
        width: 100%;
        height: 300px;
        margin-left: auto;
        margin-right: auto;
    }


    .swiper-slide {
        background-size: cover;
        background-position: center;
    }

    .gallery-top {
        height: 90%;
        width: 100%;
        margin-top: -72px;
    }

    .gallery-thumbs {
        height: 40%;
        box-sizing: border-box;
        /*padding: 10px 0;*/
        position: absolute;
        top: 15%;
        width: 200px;
    }

    .gallery-thumbs .swiper-slide {
        height: 100%;
        opacity: 0.4;
    }

    .gallery-thumbs .swiper-slide-active {
        opacity: 1;
    }

    ul.nav.nav-tabs.black, .tab-content.black
    {
        background: black;
        color: #fff;
    }

    ul.nav.nav-tabs {
        /*background: rgba(204, 204, 204, 0.85);*/
        height: 50px;
        border: none;
    }

    ul.nav.nav-tabs li a {
        height: 100%;
        vertical-align: middle;
        font-size: 20px;
    }

    ul.nav.nav-tabs li.active a {
        border-radius: 0;
        background: transparent;
        border: 0;
    }

    ul.nav.nav-tabs li.active:before
    {
        display: block;
        height: 2px;
        width: calc(100% - 2px);
        background: blue;
        content: " ";
        position: absolute;
        z-index: 99;
        bottom: 0;
    }
    .tab-content {
        height: calc(100% - 50px);
        background: rgba(255, 255, 255, 0.74);
        filter: blur(5px);
        -webkit-filter: blur(0px);
        -moz-filter: blur(5px);
        -o-filter: blur(5px);
        -ms-filter: blur(5px);
        filter: blur(5px);
        padding: 20px;
    }

    .tab-content h2 {
        font-size: 18px;
        padding: 0;
        margin: 0;
    }
    .nav-tabs>li>a {
        border-radius: 0;
    }
</style>
</head>
<body>
<?php

    // WP_Query arguments
    $args = array (
        'post_type'              => array( 'slides' ),
        'post_status'            => array( 'publish' ),
        'meta_query'             => array(
            array(
                'key'       => 'slider_image',
                'type'      => 'CHAR',
            ),
        ),
    );

    // The Query
    $query = new WP_Query( $args );



?>
<div class="swiper-container gallery-top">
    <div class="swiper-wrapper">
        <?php
        if($query->have_posts()):
            while($query->have_posts()): $query->the_post(); ?>

                <div class="swiper-slide <?php echo the_ID();?>">
                    <img
                        src="<?php echo get_bloginfo('template_directory') . '/resources/libs/timthumb.php?src=' . post_custom('slider_image') . '&w=1200' ?>"
                        alt="">
                    <div class="text">
                        <!-- <h1><?php the_title(); ?></h1> -->
                        <!-- <p><?php the_excerpt(); ?></p> -->
                        <a class="btn btn-primary" href="<?php the_permalink(); ?>">
                            Find out more
                        </a>
                    </div>

                </div>
            <?php
            endwhile;
        endif;
        wp_reset_postdata();
        ?>
    </div>
    <div class="container">
        <div class="swiper-container gallery-thumbs">
            <div class="swiper-wrapper">
                <?php while($query->have_posts()): $query->the_post() ?>
                    <div class="swiper-slide jessun">
                        <h4><?php the_title(); ?></h4>
                        <span><?php the_excerpt(); ?></span>
                    </div>

                <?php endwhile; wp_reset_query();?>
            </div>
        </div>
        <div class="swiper-button-next swiper-button-white"></div>
        <div class="swiper-button-prev swiper-button-white"></div>
    </div>
    <div style="
    position: absolute;
    top: 60%;
    background: rgba(255, 255, 255, 0.74);
    width: 100%;
    z-index: 666;
    height: 40%;
    ">
        <div class="container">
            <div class="col-md-9">
                <div class="row">
                    <ul class="nav nav-tabs" role="tablist">
                    <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'tabs',
                        'menu_class'        => 'nav nav-tabs',
    //            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                        'walker'            => new BootstrapTabsWalker(),
                        'items_wrap' => '<ul class="nav nav-tabs" role="tablist">%3$s</ul>',
                        'container'     => '',
                        'depth' => 1
                    ) );


//                    foreach(wp_get_nav_menu_items('Slider Tabs',array(
//                        'menu_item_parent'=> 0
//                    )) as $menu):
//                        echo "<li role='presentation'  ><a href='#{$menu->title}' aria-controls='{$menu->title}' role='tab' data-toggle='tab'>{$menu->title}</a></li>";
//                    endforeach;
                    ?>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="active tab-pane" aria-expanded="true" id="services">
                            <h3 style="margin-top: 10px;">Business Services</h3>
                            <p>
                                Our business services include advice and guidance for

                                existing or aspiring new businesses in:
                            </p>

                            <ul>
                                <li>Startup during the first years in business</li>
                                <li>Becoming established through growth and expansion</li>
                                <li>Innovation in developing new products and market strategies</li>
                                <li>Troubleshooting and dealing with downturns</li>
                                <li>Planning for the future, including exit planning</li>
                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="business-innovation">
                            <div class="">
                                <div class="row">
                                    <?php
                                    if($menus = wp_get_nav_menu_items('BusinessInnovations',array(
                                        'menu_item_parent'=> 0,
                                        'posts_per_page'=> 6

                                    ))):
                                        foreach($menus as $menu):
                                            ?>
                                            <div class="col-md-2">
                                                <h2><a href="<?php echo $menu->url; ?>"><?php echo $menu->title; ?></a></h2>
                                                <i><?php echo $menu->description; ?></i>
                                                <div class="tab-submenu">
                                                    <?php
                                                    foreach(wp_get_nav_menu_items('BusinessInnovations',array(
                                                        'menu_item_parent'=> 0
                                                    )) as $menu):
                                                        ?>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </div>

                                <!-- <div class="col-md-2">
                                    <h2><a href="#">Insurance</a></h2>
                                    <div class="tab-submenu">
                                        <a href="#">Life Insurance</a><br>
                                        <a href="#">Disability Cover</a>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h2><a href="#">Insurance</a></h2>
                                    <div class="tab-submenu">
                                        <a href="#">Life Insurance</a><br>
                                        <a href="#">Disability Cover</a>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h2><a href="#">Insurance</a></h2>
                                    <div class="tab-submenu">
                                        <a href="#">Life Insurance</a><br>
                                        <a href="#">Disability Cover</a>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <h2><a href="#">Insurance</a></h2>
                                    <div class="tab-submenu">
                                        <a href="#">Life Insurance</a><br>
                                        <a href="#">Disability Cover</a>
                                    </div>
                                </div> -->
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-3">
                <div class="row">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs black" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Trainings</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Contact</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content black">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('service_tabs')) : else : ?>
                                <div class="pre-widget">
                                </div>
                            <?php endif; ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>



<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/bower_components/Swiper/dist/js/swiper.min.js"></script>
<script>
    var galleryTop = new Swiper('.gallery-top', {
        initialSlide: 1,
        autoplay: 3000,
        autoplayDisableOnInteraction: true,
        //simulateTouch: false,
        loop:true,
        loopedSlides: 8,
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {

        prevButton: '.swiper-button-prev',
        nextButton: '.swiper-button-next',
        slidesPerView: 3,
        autoplay: 3000,
        autoplayDisableOnInteraction: true,
        //simulateTouch: false,
        touchRatio: 0.2,
        centeredSlides:true,
        loop:true,
        initialSlide: 1,
        loopedSlides: 8, //looped slides should be the same
        slideToClickedSlide: true,
        direction:'vertical',

    });
    galleryTop.params.control = galleryThumbs;
    galleryThumbs.params.control = galleryTop;

</script>

