<?php

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/17/16
 * Time: 10:45 PM
 */
require_once 'widgets/Slider.php';
class Slider
{
    /**
     * Slider constructor.
     */
    public function __construct()
    {
        $this->register_handlers();
        add_action('admin_menu', array($this, 'create_meta_box'));
    }

    private function register_handlers()
    {
        register_post_type('slides',
            array(
                'labels' => array(
                    'name' => __( 'Slides' ),
                    'singular_name' => __( 'Slide' ),
                    'add_new_item' => 'Add New Slide'
                ),
                'public' => true,
                'supports' => array('title','media' ,'editor','excerpt'),
                'has_archive' => true,
                'menu_icon' => get_template_directory_uri().'/resources/libs/Slider/resources/icon.png',
                'show_ui'=>true,
                'rewrite' => array('slug' => 'slide'),
            )
        );
        add_action('post_edit_form_tag', array($this,'add_form_file_support'));
        add_action('save_post', array($this,'save'), 12);

        add_filter("manage_edit-slides_columns", array($this,"override_default_columns"));
        add_action("manage_slides_posts_custom_column", array($this,"push_slides_columns"), 10,2);

    }

    function override_default_columns()
    {
        $columns = array(
            'cb'	 	=> '<input type="checkbox" />',
            'title' 	=> 'Title',
            'image'	=>	'Slide',
        );
        return $columns;
    }
    function push_slides_columns($column)
    {
        global $post;
        switch ($column) {
            case 'image':
                echo '<img src="'.get_bloginfo('template_directory').'/resources/libs/timthumb.php?src='.$post->slider_image.'&w=300" alt="" />';
                break;
        }
    }

    function create_meta_box()
    {
        add_meta_box( 'new-meta-boxes', 'Slide Image', array($this,'new_meta_boxes'), 'slides', 'normal', 'high' );


    }
    function save($id){
        global $post;
        if($post->post_type == 'slides' && $_FILES['slide_upload']['name']!='')
        {
            $upload = wp_upload_bits($_FILES['slide_upload']['name'], null, file_get_contents($_FILES['slide_upload']['tmp_name']));
            if(isset($upload['error']) && $upload['error'] != 0) {
                wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
            } else {
                update_post_meta($id, 'slider_image', $upload['url']);
            }
        }
    }

    function add_form_file_support() {
        global $post;
        if($post->post_type == 'slides') {
            echo ' enctype="multipart/form-data"';
        }
    }

    function new_meta_boxes() {
        wp_nonce_field(plugin_basename(__FILE__), 'slide_upload_nonce');
        global $post;
        $meta_box_value = get_post_meta($post->ID,'slider_image',true);
        if($meta_box_value) { echo '<img src="'.get_bloginfo('template_directory').'/resources/libs/timthumb.php?src='.$meta_box_value.'&w=120&h=120" alt="" />'; }

        echo'<p><label for="slide_upload">Slide Image</label>';
        echo'<input type="file" name="slide_upload" size="55" /><br />';
        echo'<small>Upload image here</small></p>';
    }
}