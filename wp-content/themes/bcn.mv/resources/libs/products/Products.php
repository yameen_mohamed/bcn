<?php

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/28/16
 * Time: 10:42 AM
 */
class Product
{

    /**
     * YouTube constructor.
     */
    public function __construct()
    {
        $this->register_handlers();

        $this->products_taxonomy();

        add_theme_support('post-thumbnails');
    }


    private function register_handlers()
    {
        register_post_type('products',
            array(
                'labels' => array(
                    'name' => __( 'Products' ),
                    'singular_name' => __( 'Product' ),
                    'add_new_item' => 'Add New Product'
                ),
//                'taxonomies' => array('category'),
                'hierarchical'=>'false',
                'public' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => true,
                'query_var' => true,
                'supports' => array('title','media' ,'editor','excerpt','thumbnail'),
                'has_archive' => true,
                'menu_icon' => get_template_directory_uri().'/resources/libs/products/resources/icon.png',
                'show_ui'=>true,
                'rewrite' => array('slug' => 'products'),
            )
        );
//        add_action('post_edit_form_tag', array($this,'add_form_file_support'));
        add_action('save_post', array($this,'save'), 12);

        add_filter("manage_edit-products_columns", array($this,"override_default_columns"));
        add_action("manage_products_posts_custom_column", array($this,"push_products_columns"), 10,2);





    }

    function products_taxonomy() {
        register_taxonomy(
            'products_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'products',        //post type name
            array(
                'hierarchical' => true,
                'label' => 'Product Types',  //Display name
                'public' => true,
                'query_var' => true,
                'rewrite' => array(
                    'slug' => 'product_type', // This controls the base slug that will display before each term
                    'with_front' => false, // Don't display the category base before
                )
            )
        );
    }

    function override_default_columns()
    {
        $columns = array(
            'cb'	 	=> '<input type="checkbox" />',
            'title' 	=> 'Title',
            'category'  => 'Category',
            'image'	=>	'Product Preview',
        );
        return $columns;
    }
    function push_products_columns($column)
    {
        global $post;
        switch ($column) {
            case 'image':
                echo $post->embed_code;
                break;
            case 'category':
                echo implode(', ', array_map(function($a){
                    return $a->name;
                }, wp_get_post_terms($post->ID,'products_categories')));
                break;
        }
    }


    function save($id){
        global $post;
        if($post->post_type == 'products' && $_POST['embed_code'] !='')
        {
            update_post_meta($id, 'embed_code', $_POST['embed_code']);
        }
    }


}