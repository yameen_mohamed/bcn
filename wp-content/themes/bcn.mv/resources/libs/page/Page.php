<?php

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 3/28/16
 * Time: 11:37 AM
 */
class Page
{


    /**
     * Page constructor.
     */
    public function __construct()
    {
        $this->page_taxonomy();
    }

    function page_taxonomy() {
        register_taxonomy(
            'page_group',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'page',        //post type name
            array(
                'hierarchical' => true,
                'label' => 'Page Group',  //Display name
                'public' => true,
                'query_var' => true,
                'rewrite' => array(
                    'slug' => 'page_group', // This controls the base slug that will display before each term
                    'with_front' => false, // Don't display the category base before
                )
            )
        );
    }
}