<?php

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/28/16
 * Time: 10:42 AM
 */
class Resource
{

    /**
     * YouTube constructor.
     */
    public function __construct()
    {
        $this->register_handlers();
        add_action('admin_menu', array($this, 'create_meta_box_video'));

        $this->resources_taxonomy();

    }


    private function register_handlers()
    {
        register_post_type('resources',
            array(
                'labels' => array(
                    'name' => __( 'Resources' ),
                    'singular_name' => __( 'Resource' ),
                    'add_new_item' => 'Add New Resource'
                ),
//                'taxonomies' => array('category'),
                'hierarchical'=>'false',
                'public' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => true,
                'query_var' => true,
                'supports' => array('title','media' ,'editor','excerpt'),
                'has_archive' => true,
                'menu_icon' => get_template_directory_uri().'/resources/libs/resources/resources/icon.png',
                'show_ui'=>true,
                'rewrite' => array('slug' => 'resources'),
            )
        );
//        add_action('post_edit_form_tag', array($this,'add_form_file_support'));
        add_action('save_post', array($this,'save'), 12);

        add_filter("manage_edit-resources_columns", array($this,"override_default_columns"));
        add_action("manage_resources_posts_custom_column", array($this,"push_resources_columns"), 10,2);



        function my_add_attachment_location_field( $form_fields, $post ) {
            $field_value = get_post_meta( $post->ID, 'location', true );
            $form_fields['location'] = array(
                'value' => $field_value ? $field_value : '',
                'label' => __( 'Location' ),
                'helps' => __( 'Set a location for this attachment' )
            );
            return $form_fields;
        }
        add_filter( 'attachment_fields_to_edit', 'my_add_attachment_location_field', 10, 2 );




        add_action('save_post', 'save_pdf_link');
        
        
        function save_pdf_link(){
                global $post;
                if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){ return $post->ID; }
                update_post_meta($post->ID, "link", $_POST["link"]);
        }
        add_action( 'admin_head', 'pdf_css' );
        function pdf_css() {
                echo '<style type="text/css">
                .pdf_select{
                        font-weight:bold;
                        background:#e5e5e5;
                        }
                .pdf_count{
                        font-size:9px;
                        color:#0066ff;
                        text-transform:uppercase;
                        background:#f3f3f3;
                        border-top:solid 1px #e5e5e5;
                        padding:6px 6px 6px 12px;
                        margin:0px -6px -8px -6px;
                        -moz-border-radius:0px 0px 6px 6px;
                        -webkit-border-radius:0px 0px 6px 6px;
                        border-radius:0px 0px 6px 6px;
                        }
                .pdf_count span{color:#666;}
                        </style>';
        }
        function pdf_file_url(){
                global $wp_query;
                $custom = get_post_custom($wp_query->post->ID);
                echo $custom['link'][0];
        }

    }


    function pdf_link(){
            global $post;
            $custom  = get_post_custom($post->ID);
            $link    = $custom["link"][0];
            $count   = 0;
            echo '<div class="link_header">';
            $query_pdf_args = array(
                    'post_type' => 'attachment',
                    'post_mime_type' =>'application/pdf',
                    'post_status' => 'inherit',
                    'posts_per_page' => -1,
                    );
            $query_pdf = new WP_Query( $query_pdf_args );
            $pdf = array();
            echo '<select name="link">';
            echo '<option class="pdf_select">Choose a file</option>';
            foreach ( $query_pdf->posts as $file) {
               if($link == $pdf[]= $file->guid){
                  echo '<option value="'.$pdf[]= $file->guid.'" selected="true">'.$pdf[]= $file->post_title.'</option>';
                     }else{
                  echo '<option value="'.$pdf[]= $file->guid.'">'.$pdf[]= $file->post_title.'</option>';
                     }
                    $count++;
            }
            echo '</select><br /></div>';
            echo '<p>Select a pdf file from the above list to attach to this post.</p>';
            echo '<div class="pdf_count"><span>Files:</span> <b>'.$count.'</b></div>';
    }

    function resources_taxonomy() {
        register_taxonomy(
            'resources_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'resources',        //post type name
            array(
                'hierarchical' => true,
                'label' => 'Resource Types',  //Display name
                'public' => true,
                'query_var' => true,
                'rewrite' => array(
                    'slug' => 'resource_type', // This controls the base slug that will display before each term
                    'with_front' => false, // Don't display the category base before
                )
            )
        );
    }

    function override_default_columns()
    {
        $columns = array(
            'cb'	 	=> '<input type="checkbox" />',
            'title' 	=> 'Title',
            'category'  => 'Category',
            'image'	=>	'Resource Preview',
        );
        return $columns;
    }
    function push_resources_columns($column)
    {
        global $post;
        switch ($column) {
            case 'image':
                echo $post->embed_code;
                break;
            case 'category':
                echo implode(', ', array_map(function($a){
                    return $a->name;
                }, wp_get_post_terms($post->ID,'resources_categories')));
                break;
        }
    }

    function create_meta_box_video()
    {

        add_meta_box("my-pdf", "PDF Document", array($this,"pdf_link"), "resources", "normal", "low");

    }
    function save($id){
        global $post;
        if($post->post_type == 'resources' && $_POST['embed_code'] !='')
        {
            update_post_meta($id, 'embed_code', $_POST['embed_code']);
        }
    }

    function new_meta_boxes_video() {
        global $post;
        $meta_box_value = get_post_meta($post->ID,'embed_code',true);
        if($meta_box_value) { echo html_entity_decode($post->embed_code); }

        echo'<p><label for="slide_upload">YouTube Embed Code</label>';
        echo'<input type="text" name="embed_code" size="55" value="'.$post->embed_code.'" /><br />';
        echo'<small>Copy paste the embed from YouTube</small></p>';
    }
}