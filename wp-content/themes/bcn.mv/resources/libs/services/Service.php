<?php

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/29/16
 * Time: 1:14 PM
 */
class Service
{


    /**
     * YouTube constructor.
     */
    public function __construct()
    {
        $this->register_handlers();
        add_action('admin_menu', array($this, 'create_meta_box_video'));

    }


    private function register_handlers()
    {
        register_post_type('services',
            array(
                'labels' => array(
                    'name' => __( 'Services' ),
                    'singular_name' => __( 'Service' ),
                    'add_new_item' => 'Add New Service'
                ),
                'public' => true,
                'supports' => array('title','excerpt'),
                'has_archive' => true,
                'menu_icon' => get_template_directory_uri().'/resources/libs/services/resources/icon.png',
                'show_ui'=>true,
                'rewrite' => array('slug' => 'services'),
            )
        );
//        add_action('post_edit_form_tag', array($this,'add_form_file_support'));
        add_action('save_post', array($this,'save'), 12);

        add_filter("manage_edit-services_columns", array($this,"override_default_columns"));
        add_action("manage_services_posts_custom_column", array($this,"push_services_columns"), 10,2);

    }

    function override_default_columns()
    {
        $columns = array(
            'cb'	 	=> '<input type="checkbox" />',
            'title' 	=> 'Title',
            'image'	=>	'Service Preview',
        );
        return $columns;
    }
    function push_services_columns($column)
    {
        global $post;
        switch ($column) {
            case 'image':
                echo $post->embed_code;
                break;
        }
    }

    function create_meta_box_video()
    {
        add_meta_box( 'new-meta-boxes-services', 'YouTube Embed Code', array($this,'new_meta_boxes_video'), 'services', 'normal', 'high' );


    }
    function save($id){
        global $post;
        if($post->post_type == 'services' && $_POST['embed_code'] !='')
        {
            update_post_meta($id, 'embed_code', $_POST['embed_code']);
        }
    }

    function new_meta_boxes_video() {
        global $post;
        $meta_box_value = get_post_meta($post->ID,'embed_code',true);
        if($meta_box_value) { echo html_entity_decode($post->embed_code); }

        echo'<p><label for="slide_upload">YouTube Embed Code</label>';
        echo'<input type="text" name="embed_code" size="55" value="'.$post->embed_code.'" /><br />';
        echo'<small>Copy paste the embed from YouTube</small></p>';
    }
}