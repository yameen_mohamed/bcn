<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/29/16
 * Time: 1:22 PM
 */

class Services extends WP_Widget
{
    function __construct() {

        parent::__construct(
            'service_widget',
            __('Services', 'wpb_widget_domain'),
            array( 'description' => __( 'Slider will display slider posts! Ideal for main page, and sub-pages!', 'wpb_widget_domain' ), )
        );
    }

    // Creating widget front-end
// This is where the action happens
    public function widget( $args, $instance ) {
        include 'views/service_listing.php';
        echo $args['after_widget'];
    }

// Widget Backend
    public function forms( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }



}

// Register and load the widget
function load_service_widget() {
    register_widget( 'Services' );
}
add_action( 'widgets_init', 'load_service_widget' );

