<ul class="nav">

    <?php

    $training_list_args = array(
        'post_type' => 'trainings',
        'posts_per_page' => 5
    );

    $training_list_query = new WP_Query($training_list_args);

    if ($training_list_query->have_posts()):
        while ($training_list_query->have_posts()): $training_list_query->the_post();
            ?>
            <li>
                <a class="hoverTip" href="<?php the_permalink() ?>"> <?php the_title(); ?>
                    <div class="slideUp">
                        <span class="date">Start @<?php  echo get_the_training_start_date(); ?></span>
                    </div>
                </a>
            </li>


        <?php endwhile;endif; ?>
</ul>
