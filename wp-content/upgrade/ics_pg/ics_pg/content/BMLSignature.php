<?php

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/16/16
 * Time: 3:48 PM
 */
class BMLSignature
{
    private $password;
    private $marId;
    private $acqId;
    private $orderId;
    private $amount;
    private $currency;


    /**
     * BMLSignature constructor.
     * @param $password
     * @param $marId
     * @param $acqId
     * @param $orderId
     * @param $amount
     * @param $currency
     */
    public function __construct($password, $marId, $acqId, $orderId, $amount, $currency)
    {
        $this->password = $password;
        $this->marId = $marId;
        $this->acqId = $acqId;
        $this->orderId = 'MPGORDID01154321';//$orderId;
        $this->amount = $this->format_amount($amount);
        $this->currency = $currency;
    }

    /**
     * @param $amount
     * @return mixed
     */
    private function format_amount($amount)
    {
        return str_replace('.','', str_pad( number_format(doubleval($amount), 2,'.',''),13,'0',STR_PAD_LEFT) );
    }

    /**
     * @return string
     */
    public function calculateSignature()
    {
        $concat = $this->password.$this->marId.$this->acqId.$this->orderId.$this->amount.$this->currency;
        return base64_encode(sha1($concat, true));
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }



}
