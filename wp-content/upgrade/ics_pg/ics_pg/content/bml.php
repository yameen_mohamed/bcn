<form action="https://ebanking.bankofmaldives.com.mv/bmlmpiuat/threed/MPI" method="post">
	<input type="hidden" value="1.0.0" name="Version" id="Version">
	<input type="hidden" value="<?php echo get_option('ics_pg_merchant_id'); ?>" name="MerID" id="MerID">
	<input type="hidden" value="<?php echo get_option('ics_pg_acquirer_id'); ?>" name="AcqID" id="AcqID">
	<input type="hidden" value="<?php echo get_option('ics_pg_cb_url'); ?>" name="MerRespURL" id="MerRespURL">
	<input type="hidden" value="462" name="PurchaseCurrency" id="PurchaseCurrency">
	<input type="hidden" value="2" name="PurchaseCurrencyExponent" id="PurchaseCurrencyExponent">
	<input type="hidden" value="<?php echo $bml->getOrderId() ?>" name="OrderID" id="OrderID">
	<input type="hidden" value="SHA1" name="SignatureMethod" id="SignatureMethod">
	<input type="hidden" value="<?php echo $bml->getAmount(); ?>" name="PurchaseAmt" id="PurchaseAmt">
	<input type="hidden" value="https://ebanking.bankofmaldives.com.mv/bmlmpiuat/threed/MPI" name="Url" id="Url">
	<input type="hidden" value="<?php echo $bml->calculateSignature(); ?>" name="Signature" id="Signature">

	<h2>Payment Agreement</h2>               
	<p>Bank of Maldives card holders
	<br>(American Express, VISA Debit...)
	</p>
    <input type="submit" name="submit" value="Agree &amp; Continue" class="btn btn-primary btn-lg">
</form>