<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5@lOr:EoirZ:(7R<}(gDN|;NP}OEc7*Hv[:Q5kU0LYPMiM~sYCq7,F$gt~]#9H!Y');
define('SECURE_AUTH_KEY',  'JJM?X,u#$&i+t><0jP>_d@6ooVh,k>/8I3;{#w|m~1#aAY@5ac;fHT)|1Dbj-^F%');
define('LOGGED_IN_KEY',    'K%%S2]ZX]vzV+V9FMIo,y[>F6gI1L&3Cs:^g&<NSjlp-rD(2P|tGWn}k%r^1mC`B');
define('NONCE_KEY',        'cHHpcTr4G~OY&Rm#2ne4EK-D7:ox]-p:f-[AC1.Vg|p3cw-n3|@tnLb#_Wv&X=mi');
define('AUTH_SALT',        '<TAW]:[;-`v%lkB#&}>|K%$Uy*N_#M2P@Cz>IM-RF.;tl/Gh|Zae)h=% |BvVdzh');
define('SECURE_AUTH_SALT', '17+-FC_V4IFWf;PM(k?^[AF$pXy@Ra$||t$#XR;1BI&Z3eP/^oN6?xW)mdVV+.Fx');
define('LOGGED_IN_SALT',   '%;@Z-sv}:Og b@Ac-?8/v1m F<HR`:*LA(<A8g?#P)QP$1]Bi&*<^6zJih@9-(lr');
define('NONCE_SALT',       'a_vV-uiVm-1f:XsZdd*DXc9V/*n2aU Pqv3O#aUkg-B}7M8~CDDk}3o9Af+]NFU7');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


